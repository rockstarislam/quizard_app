import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Loading } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

/**
 * Generated class for the GivecodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-givecode',
  templateUrl: 'givecode.html',
})
export class GivecodePage {

  reset = {token: '',email: '',password: '',password_confirmation: ''};
  loading: Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private http: HttpProvider,
              private auth: AuthServiceProvider,) {
  }

   back() {
  	this.navCtrl.pop();
  }
    public resetPassword() {
    if(this.reset.email && this.reset.token && this.reset.password){
    this.http.reset(this.reset).subscribe(res => {
          if (res['status'] === 'success') {
          this.navCtrl.push(LoginPage);
          this.presentToast('Your Password Reset Successfully');
        }else{
        	this.presentToast('Your Credential Does Not Matched');
        }
      },
      err => {
             this.loading.dismiss();
             const toast = this.toastCtrl.create({
            message: err.error.error,
            duration: 3000
          });
          toast.present();
      });
    }
    else{
      this.loading.dismiss();
        this.presentToast('Wrong Credential');
    }
  }

    presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000
    });
    toast.present();
  }

    showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present()
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GivecodePage');
  }

}
