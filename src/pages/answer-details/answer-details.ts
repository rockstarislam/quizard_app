import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import {QuestionPage} from '../question/question';
import {ProfilePage} from '../profile/profile';
import { SearchresultPage } from '../searchresult/searchresult';

/**
 * Generated class for the AnswerDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-answer-details',
  templateUrl: 'answer-details.html',
})
export class AnswerDetailsPage {
  data: any;
    img1: any;
  searchData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public http: HttpProvider) {
  	this.searchData = [];
     if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }
    this.data = this.navParams.data;
  }

       searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }
   profile() {
    this.navCtrl.push(ProfilePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnswerDetailsPage');
  }

}
