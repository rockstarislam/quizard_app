import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GivecodePage } from './givecode';

@NgModule({
  declarations: [
    GivecodePage,
  ],
  imports: [
    IonicPageModule.forChild(GivecodePage),
  ],
})
export class GivecodePageModule {}
