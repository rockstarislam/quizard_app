import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';

import {IonicImageViewerModule} from 'ionic-img-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';
/**Modules**/
import {ArticlePageModule} from '../pages/article/article.module';
import {ArticleDetailPageModule} from '../pages/article-detail/article-detail.module';
import {InfographicePageModule} from '../pages/infographice/infographice.module';
import {InfographicDetailPageModule} from '../pages/infographic-detail/infographic-detail.module';
import {QuizPageModule} from '../pages/quiz/quiz.module';
import {QuestionPageModule} from '../pages/question/question.module';
import {LeaderBoardPageModule} from '../pages/leader-board/leader-board.module';
import {ScorePageModule} from '../pages/score/score.module';
import {LoginPageModule} from '../pages/login/login.module';
import {RegistrationPageModule} from '../pages/registration/registration.module';
import {ProfilePageModule} from '../pages/profile/profile.module';
import {SubjectListPageModule} from '../pages/subject-list/subject-list.module';
import {AnswerDetailsPageModule} from '../pages/answer-details/answer-details.module';
import { ForgetpasswordPageModule } from '../pages/forgetpassword/forgetpassword.module';
import { SearchresultPageModule } from '../pages/searchresult/searchresult.module';
import { GivecodePageModule } from '../pages/givecode/givecode.module';

/*end*/
import {MyApp} from './app.component';

import {SearchComponent} from '../components/search/search';

/*import {SettingsPage} from '../pages/settings/settings';
 import {LoginPage} from '../pages/login/login';
 import {RegistrationPage} from '../pages/registration/registration';
 import {ProfilePage} from '../pages/profile/profile';
 import {ArticlePage} from '../pages/article/article';
 import {InfographicePage} from '../pages/infographice/infographice';
 import {LeaderBoardPage} from '../pages/leader-board/leader-board';
 import {SubjectListPage} from '../pages/subject-list/subject-list';
 import { QuizPage } from '../pages/quiz/quiz';
 import { QuestionPage } from '../pages/question/question';
 import { ScorePage } from '../pages/score/score';
 import { ArticleDetailPage } from '../pages/article-detail/article-detail';
 import { InfographicDetailPage } from '../pages/infographic-detail/infographic-detail';*/

import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {HttpProvider} from '../providers/http/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { PhotoLibrary } from '@ionic-native/photo-library';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    ArticleDetailPageModule,
    ArticlePageModule,
    InfographicDetailPageModule,
    InfographicePageModule,
    QuestionPageModule,
    QuizPageModule,
    LeaderBoardPageModule,
    SubjectListPageModule,
    ProfilePageModule,
    ScorePageModule,
    LoginPageModule,
    RegistrationPageModule,
    AnswerDetailsPageModule,
    ForgetpasswordPageModule,
    SearchresultPageModule,
    GivecodePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    SocialSharing,
    HttpProvider,
     FileTransfer,
     File,
     PhotoLibrary
  ]
})
export class AppModule {
}
