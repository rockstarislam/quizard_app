import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import {TabsPage} from '../tabs/tabs';
/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  user: any;
  loading: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              public http: HttpProvider) {
    this.user = {name: '', email: '', password: ''};
  }

  ionViewDidLoad() {

  }

  public registration() {
    this.showLoading();
    this.http.register(this.user).subscribe(result => {
      if(result['status'] ==='success'){
        localStorage.setItem('user', JSON.stringify(result['data']));
        this.navCtrl.push(TabsPage);
      }
      this.loading.dismiss();
    }, err => {
      //this.presentAlert('Alert', err.error.errors.email[0]);
       const toast = this.toastCtrl.create({
            message: err.error.errors.email[0],
            duration: 3000
          });
          toast.present();
      this.loading.dismiss();
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

}
