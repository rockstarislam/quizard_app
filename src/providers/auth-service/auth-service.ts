import {Injectable, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library';

/*
 Generated class for the AuthServiceProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class AuthServiceProvider {
  is_played = new EventEmitter<boolean>();

  constructor(private photoLibrary: PhotoLibrary, public alertCtrl: AlertController) {
  }



  public getUserInfo() {
    const driver = JSON.parse(localStorage.getItem('user'));
    return driver;
  }

  setplayedScoreEmit(data){
    this.is_played.emit(data);
  }

    public downloadPhoto(url){
    this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.saveImage(url, 'Quizards').then(res =>{
        this.presentAlert();
      }, err=>{
        console.log(err);
      });
    });
  }

    presentAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Infographic Image Downloaded',
      buttons: ['OK']
    });
    alert.present();
  }


  public logout() {
  }

  handleError(error) {
    return Observable.throw(error.json().error || 'Server error');
  }

}

