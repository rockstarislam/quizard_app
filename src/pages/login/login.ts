import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController, LoadingController, Loading} from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import {RegistrationPage} from '../registration/registration';
import { ForgetpasswordPage } from '../forgetpassword/forgetpassword';
import {TabsPage} from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  registerCredentials = {email: '', password: ''};

  constructor(private nav: NavController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController, private http: HttpProvider) {
  }

  ionViewDidLoad() {
    if(localStorage.getItem('user')){
     this.nav.push(TabsPage);
    }
  }

  public createAccount() {
    this.nav.push('RegisterPage');
  }

  public login() {
    this.showLoading();
    this.http.login(this.registerCredentials).subscribe(res => {
        if (res['status'] === 'success') {
          localStorage.setItem('user', JSON.stringify(res['data']));
          localStorage.setItem('profile_pic', res['data'].profile_image);
          this.loading.dismiss();
          this.nav.setRoot(TabsPage);
        }
      },
      err => {
        this.loading.dismiss();
        //this.presentAlert('', err.error.error);
             const toast = this.toastCtrl.create({
            message: err.error.error,
            duration: 3000
          });
          toast.present();
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present()
  }

  navigateToOtherPage(): void {
    this.nav.push(RegistrationPage);
  }

  fogotpass(): void {
    this.nav.push(ForgetpasswordPage);
  }
}
