import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http'
import { InfographicDetailPage } from '../infographic-detail/infographic-detail';
import { SearchresultPage } from '../searchresult/searchresult';
import {ProfilePage} from '../profile/profile';
import { QuestionPage } from '../question/question';
import { ImageViewerController } from 'ionic-img-viewer';

/**
 * Generated class for the InfographicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infographice',
  templateUrl: 'infographice.html',
})
export class InfographicePage {
  left: any;
  right: any;
  data: any;
  public topic = '1';
  arr: any;
  loading: any;
  img1: any;
  searchText: any;
  searchData: any;
  _imageViewerCtrl:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: HttpProvider, public loadingCtrl: LoadingController, public imageViewerCtrl: ImageViewerController) {
    this.searchData = [];
    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }

    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
    this.getInfoGrapichData(this.topic);
  }

  infographDetail(item) {
    this.navCtrl.push(InfographicDetailPage, {
      'infoGraphData':item
    });
  }

    searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }


  profile() {
    this.navCtrl.push(ProfilePage);
  }


  getInfoGrapichData(cid) {
    this.arr = [];
    this.showLoading();
    this.http.getInfoGraphic(cid).subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      let c = 3;
      for(let i = 0; i< Number((this.data.length/3).toFixed()); i++){
        const data  = this.data.slice(i*3,c);
        if(data.length === 3){
          let left = data.slice(0,2);
          let r8 = data.slice(2,3);
          let d = {left:left, right: r8};
          this.arr.push(d);
        }else{
          let left = data.slice(0,2);
          let d = {left:left, right: []};
          this.arr.push(d);
        }
        c = c+ 3;
      }
    });
  }

    doRefresh(refresher) {
    this.arr = [];
    this.showLoading();
    this.http.getInfoGraphic(refresher).subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      refresher.complete();
      let c = 3;
      for(let i = 0; i< Number((this.data.length/3).toFixed()); i++){
        const data  = this.data.slice(i*3,c);
        if(data.length === 3){
          let left = data.slice(0,2);
          let r8 = data.slice(2,3);
          let d = {left:left, right: r8};
          this.arr.push(d);
        }else{
          let left = data.slice(0,2);
          let d = {left:left, right: []};
          this.arr.push(d);
        }
        c = c+ 3;
      }
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
   //   this.searchData.push(output.data);
    });
  }

}
