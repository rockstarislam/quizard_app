import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { QuestionPage } from '../question/question';
import { ProfilePage } from '../profile/profile';
import { SearchresultPage } from '../searchresult/searchresult';

/**
 * Generated class for the QuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {
  left: any;
  right: any;
  data: any;
  public topic = 1;
  arr: any;
  loading: any;
  img1: any;
  searchData: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: HttpProvider,
              private loadingCtrl: LoadingController) {
    this.searchData = [];
  }

  ionViewDidLoad() {
    this.getQ1();
    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }
  }



  getQ1() {
    this.arr = [];
    this.showLoading();
    this.http.getLatestQuiz().subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      let c = 3;
      for (let i = 0; i <= Number((this.data.length / 3).toFixed()); i++) {
        const data = this.data.slice(i * 3, c);
        if (data.length === 3) {
          let left = data.slice(0, 2);
          let r8 = data.slice(2, 3);
          let d = {left: left, right: r8};
          this.arr.push(d);
        } else {
          let left = data.slice(0, 2);
          let d = {left: left, right: []};
          this.arr.push(d);
        }
        c = c + 3;
      }
    }, () => {
      this.loading.dismiss();
    });
  }

  getQ2() {
    this.arr = [];
    this.showLoading();
    this.http.getMostViewedQuiz().subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      let c = 3;
      for (let i = 0; i <= Number((this.data.length / 3).toFixed()); i++) {
        const data = this.data.slice(i * 3, c);
        if (data.length === 3) {
          let left = data.slice(0, 2);
          let r8 = data.slice(2, 3);
          let d = {left: left, right: r8};
          this.arr.push(d);
        } else {
          let left = data.slice(0, 2);
          let d = {left: left, right: []};
          this.arr.push(d);
        }
        c = c + 3;
      }
    }, () => {
      this.loading.dismiss();
    });
  }

  getQ3() {
    this.arr = [];
    this.showLoading();
    const user = JSON.parse(localStorage.getItem('user'));
    this.http.getUnseenQuiz(user.id).subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      let c = 3;
      for (let i = 0; i <= Number((this.data.length / 3).toFixed()); i++) {
        const data = this.data.slice(i * 3, c);
        if (data.length === 3) {
          let left = data.slice(0, 2);
          let r8 = data.slice(2, 3);
          let d = {left: left, right: r8};
          this.arr.push(d);
        } else {
          let left = data.slice(0, 2);
          let d = {left: left, right: []};
          this.arr.push(d);
        }
        c = c + 3;
      }
    }, () => {
      this.loading.dismiss();
    });
  }

  doRefresh(refresher) {
    this.arr = [];
    this.showLoading();
    this.http.getLatestQuiz().subscribe(result => {
      this.loading.dismiss();
      this.data = result;
      refresher.complete();
      let c = 3;
      for (let i = 0; i <= Number((this.data.length / 3).toFixed()); i++) {
        const data = this.data.slice(i * 3, c);
        if (data.length === 3) {
          let left = data.slice(0, 2);
          let r8 = data.slice(2, 3);
          let d = {left: left, right: r8};
          this.arr.push(d);
        } else {
          let left = data.slice(0, 2);
          let d = {left: left, right: []};
          this.arr.push(d);
        }
        c = c + 3;
      }
    }, () => {
      this.loading.dismiss();
    });
  }


  startGame(item) {
    localStorage.setItem('current_quiz',JSON.stringify(item));
    console.log(item);
    this.navCtrl.push(QuestionPage, item);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

     searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }

  profile() {
    this.navCtrl.push(ProfilePage);
  }


}
