import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {HttpProvider} from '../../providers/http/http';
import {ProfilePage} from '../profile/profile';
import { SearchresultPage } from '../searchresult/searchresult';
import {QuestionPage} from '../question/question';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

/**
 * Generated class for the LeaderBoardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leader-board',
  templateUrl: 'leader-board.html',
})
export class LeaderBoardPage {
  data: any;
  loading: any;
  img1: any;
  searchData: any;
  constructor(public navCtrl: NavController,
              public auth: AuthServiceProvider,
              public navParams: NavParams,
              public http: HttpProvider,
              public loadingCtrl: LoadingController) {
    this.searchData = [];
  }

  profile() {
    this.navCtrl.push(ProfilePage);
  }

  ionViewDidLoad() {
    this.getPlayedQuiz();
    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }
  }

  getPlayedQuiz() {
    this.showLoading();
    this.http.playedQuiz().subscribe(result =>{
      this.loading.dismiss();
      this.data = result;
      this.auth.setplayedScoreEmit(true);
    });
  }

  
  doRefresh(refresher) {
    this.http.playedQuiz().subscribe(result => {
      this.data = result;
      refresher.complete();
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

   searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }
}
