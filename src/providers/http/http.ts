import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {config} from '../../config';
import {Observable} from 'rxjs/Rx';

/*
 Generated class for the HttpProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class HttpProvider {
  public API = config.API;

  constructor(public httpClient: HttpClient) {

  }


  public getArticles(cid) {
    return this.httpClient.get(this.API + 'article');
  }

  public getInfoGraphic(cid) {
    return this.httpClient.get(this.API + 'infographic');
  }

    public getQuizById(id) {
    return this.httpClient.get(this.API + 'search-quiz/'+id);
  }

  public getLatestQuiz() {
    return this.httpClient.get(this.API + 'latest-quiz');
  }

  public getMostViewedQuiz() {


    return this.httpClient.get(this.API + 'mostviewed-quiz');

  }

  public getUnseenQuiz(user_id) {
    return this.httpClient.get(this.API + 'unseen-quiz/' + user_id);
  }


  public getQuestions() {
    return this.httpClient.get(this.API + 'quiz');
  }

  public register(data) {
    return this.httpClient.post(this.API + 'auth/register', data);
  }

  public login(data) {
    return this.httpClient.post(this.API + 'login', data);
  }

  public playedQuiz() {
    return this.httpClient.get(this.API + 'played-quiz-score');
  }

  public getGameSummary(user_id) {
    return this.httpClient.get(this.API + 'percentage-of-success/' + user_id);
  }

  public saveQ(data) {
    return this.httpClient.post(this.API + 'save-played-quiz-score', data);
  }

  public demo() {
    return this.httpClient.get('assets/information.json');
  }

  public resEmail(data) {
    return this.httpClient.post(this.API + 'password_reset', data);
  }

  public reset(data) {
    return this.httpClient.post(this.API + 'reset', data);
  }


  public changeName(data) {
    return this.httpClient.post(this.API + 'update-name', data);
  }

  public changeEmail(data) {
    return this.httpClient.post(this.API + 'update-email', data);
  }

  public updateProfilePic(data) {
    return this.httpClient.post(this.API + 'update-profile-pic', data);
  }

  public changePassword(data) {
    return this.httpClient.post(this.API + 'update-password', data);
  }

  public getHighestPoint(id) {
    return this.httpClient.get(this.API + 'highest-point/' + id);
  }

  public getTopScoreByQuizId(qid) {
    return this.httpClient.get(this.API + 'leaderboard-onlythis-quiz/' + qid);
  }

  updateQuizViewStatus(qid) {
    return this.httpClient.post(this.API + 'quiz-visitor-count/' + qid, {});
  }

  searchApi(str: any) {
    return this.httpClient.get(this.API + 'search/' + str);
  }

  public getUserPlayedLastQuiz(user_id) {
    return this.httpClient.get(this.API + 'played-last-three-quiz/' + user_id);
  }

  uploadFile(data) {
/*    let headers =  new HttpHeaders({
      'Content-Type':'multipart/form-data',
      'X-Requested-With':'XMLHttpRequest'
    });*/
    //headers.append('Content-Type', 'multipart/form-data');
   // headers.append('X-Requested-With', 'XMLHttpRequest');

    return this.httpClient.post('https://api.cloudinary.com/v1_1/devoret/image/upload', data);
  }

  handleError(error) {
    return Observable.throw(error.json().error || 'Server error');
  }
}
