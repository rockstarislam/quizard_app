import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import {RoundProgressModule} from 'angular-svg-round-progressbar';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    RoundProgressModule,
    IonicPageModule.forChild(ProfilePage),
  ],
})
export class ProfilePageModule {}
