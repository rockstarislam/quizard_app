import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfographicDetailPage } from './infographic-detail';

@NgModule({
  declarations: [
    InfographicDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(InfographicDetailPage),
  ],
})
export class InfographicDetailPageModule {}
