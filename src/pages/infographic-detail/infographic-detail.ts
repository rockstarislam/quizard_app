import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Loading, LoadingController} from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the InfographicDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infographic-detail',
  templateUrl: 'infographic-detail.html',
})
export class InfographicDetailPage {
  infoGraphData: any;
  loading: Loading;
  _imageViewerCtrl: ImageViewerController;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public auth: AuthServiceProvider,
              imageViewerCtrl: ImageViewerController,
              public transfer: FileTransfer,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public file: File,
              public socialSharing: SocialSharing) {
    this.infoGraphData = this.navParams.get('infoGraphData');
    this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
  }

  fShare(item) {
    this.socialSharing.shareViaFacebook(item.description, item.image, '').then(() => {

    }).catch(() => {
    });
  }

  tShare(item) {
    this.socialSharing.shareViaTwitter(item.description, item.image, '').then(() => {

    }).catch(() => {
    });

  }

  eShare(item) {
// Share via email
    this.socialSharing.shareViaEmail(item.description, item.title, []).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

  wShare(item){
    this.socialSharing.share(item.description, item.title, item.image, '').then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }
  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

  download(imageUrl) {
    this.showLoading();
    const fileTransfer: FileTransferObject = this.transfer.create();
    const url = imageUrl;
    fileTransfer.download(url, this.file.dataDirectory + 'myfile.jpg').then((entry) => {
      this.loading.dismissAll();
      this.showToast('download complete: ' + entry.toURL());
    }, (error) => {
      this.loading.dismissAll();
      this.showToast("Sorry! cannot download");
    });
  }

  showToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Image Downloading...',
      dismissOnPageChange: false
    });
    this.loading.present()
  }

    downloadImage(url){
    this.auth.downloadPhoto(url);
  }

}
