import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import {LeaderBoardPage} from '../leader-board/leader-board';
import {AnswerDetailsPage} from '../answer-details/answer-details';
import {HttpProvider} from '../../providers/http/http';
import {ProfilePage} from '../profile/profile';
import {QuestionPage} from '../question/question';
import { SearchresultPage } from '../searchresult/searchresult';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

/**
 * Generated class for the ScorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-score',
  templateUrl: 'score.html',
})
export class ScorePage {
  obtained_score: any;
  highest_score: any;
  loading: any;
  scores: any;
  dataList: any;
  img1: any;
  searchData: any;

  constructor(public navCtrl: NavController,
              public auth: AuthServiceProvider,
              public navParams: NavParams,
              public http: HttpProvider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController) {
    this.obtained_score = this.navParams.data;
    this.saveData(this.obtained_score);
    this.getHighScore();
    this.highest_score = 0;
    this.scores = [];
    this.searchData = [];
  }

  ionViewDidLeave() {

  }

  leaderpage() {
    this.navCtrl.push(LeaderBoardPage);
  }

  qdetail(data) {
    this.navCtrl.push(AnswerDetailsPage, data);
  }

  saveData(data) {
    this.http.saveQ(data).subscribe(result => {
    });
  }

  getHighScore() {
    this.showLoading();
    this.http.getHighestPoint(this.obtained_score.quiz_id).subscribe(data => {
      this.highest_score = data;
      this.loading.dismiss();
    });
  }

  ionViewDidLoad() {
    this.highScoreList();
    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  presentAlert(title, msg) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  styleProgressBar(id) {
    if (this.obtained_score.played_question_set.length > 0) {
      const d = this.obtained_score.played_question_set.filter(item => item.id === id);
      if (d.length > 0) {
        if (d[0].isRight === 1) {
          return "active";
        }

        if (d[0].isRight === 0) {
          return "wrong";
        }
      }
    }
  }

  highScoreList() {
    this.http.getTopScoreByQuizId(this.obtained_score.quiz_id).subscribe(result => {
      this.dataList = result;
      this.auth.setplayedScoreEmit(true);
    });
  }


  doRefresh(refresher) {
    this.http.getTopScoreByQuizId(this.obtained_score.quiz_id).subscribe(result => {
      this.dataList = result;
      refresher.complete();
    });
  }

  playAgain() {
    const data = JSON.parse(localStorage.getItem('current_quiz'));
    this.navCtrl.push(QuestionPage, data);
  }

   searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }

  profile() {
    this.navCtrl.push(ProfilePage);
  }


}
