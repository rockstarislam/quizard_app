import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, App, ToastController} from 'ionic-angular';
import {LoginPage} from '../login/login';
import {HttpProvider} from '../../providers/http/http';
import {QuestionPage} from '../question/question';
import { SearchresultPage } from '../searchresult/searchresult';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {
  user: any;
  settingOpen: any;
  settingNameChange: any;
  settingEmailChange: any;
  settingPasswordChange: any;
  imageOpen: any;
  quizOpen: any;
  logoutOpen: any;
  public current: number = 0;
  public max: number = 100;
  user_data: any;
  name: any;
  email: any;
  old_email: any;
  password: any;
  user_played_quiz: any;
  img1: any;
  searchData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _app: App,
              public http: HttpProvider, private toastCtrl: ToastController) {
    this.settingOpen = false;
    this.settingNameChange = false;
    this.settingEmailChange = false;
    this.settingPasswordChange = false;
    this.quizOpen = false;
    this.logoutOpen = false;
    this.user_data = {};
    this.searchData = [];
  }

  ionViewDidLoad() {
    this.user_played_quiz = [];
    this.email = '';
    this.user = JSON.parse(localStorage.getItem('user'));
/*        if (localStorage.getItem('game_board')) {
     const user_q = JSON.parse(localStorage.getItem('game_board'));
     this.user_played_quiz = user_q.slice(3);
     }*/
    this.old_email = this.user.email;
    this.getUserPlayedQuiz();
    this.password = '';

    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }

  }


  ngOnInit() {
  }

  UserPlayedQuiz() {
    this.http.getUserPlayedLastQuiz(this.user.id).subscribe(result => {
      this.user_played_quiz = result;
    });
  }

  toggleSection(i) {

    switch (i) {
      case 1:
        this.settingOpen = !this.settingOpen;
        break;
      case 2:
        this.quizOpen = !this.quizOpen;
        break;
      case 3:
        this.logoutOpen = !this.logoutOpen;
        break;
      case 4:
        this.settingNameChange = !this.settingNameChange;
        break;
      case 5:
        this.settingEmailChange = !this.settingEmailChange;
        break;
      case 6:
        this.settingPasswordChange = !this.settingPasswordChange;
        break;
      case 7:
        this.imageOpen = !this.imageOpen;
        break;
    }
  }

  toggleItem(i, j) {
    // this.information[i].children[j].open = !this.information[i].children[j].open;
  }

  signOut() {
    localStorage.removeItem('user');
    localStorage.removeItem('profile_pic');
    this._app.getRootNav().setRoot(LoginPage);
  }

  getOverlayStyle() {
    let isSemi = false;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': 125 / 8 + 'px'
    };
  }

  getUserPlayedQuiz() {
    this.http.getGameSummary(this.user.id).subscribe(data => {
      this.user_data = data;
      this.max = 100;
      this.current = Number(((Number(this.user_data.right_ans) * 100) / (Number(this.user_data.total_question))).toFixed(2));
      if (isNaN(this.current)) {
        this.current = 0;
      }
    });
  }

  updateName() {
    const data = {id: this.user.id, name: this.name};
    this.http.changeName(data).subscribe(res => {
      if (res['status'] === 'success') {
        localStorage.setItem('user', JSON.stringify(res['data']));
        this.user = JSON.parse(localStorage.getItem('user'));

        const toast = this.toastCtrl.create({
          message: 'Your Name has Been Updated',
          duration: 3000
        });
        toast.present();
      }

    }, err => {
      const toast = this.toastCtrl.create({
        message: 'Something went wrong!',
        duration: 3000
      });
      toast.present();
    });
  }

  // presentAlert(title, msg) {
  //   let alert = this.toastCtrl.create({
  //     title: title,
  //     subTitle: msg,
  //     buttons: ['ok']
  //   });
  //   alert.present();
  // }

  updateEmail() {
    if (this.email !== '') {
      const data = {id: this.user.id, email: this.email};
      this.http.changeEmail(data).subscribe(res => {
        if (res['status'] === 'success') {
          localStorage.setItem('user', JSON.stringify(res['data']));
          this.user = JSON.parse(localStorage.getItem('user'));

          const toast = this.toastCtrl.create({
            message: 'Your Email has Been Updated',
            duration: 3000
          });
          toast.present();

          // this.presentAlert('Success', 'Your Email has been updated!')
        }

      }, () => {
        const toast = this.toastCtrl.create({
          message: 'Something went wrong!',
          duration: 3000
        });
        toast.present();
      });
    } else {
      const toast = this.toastCtrl.create({
        message: 'Please enter new email address!',
        duration: 3000
      });
      toast.present();
      // this.presentAlert('Error', 'Please enter new email address!');
    }

  }

  updatePassword() {
    if (this.password !== '') {
      const data = {id: this.user.id, password: this.password};
      this.http.changePassword(data).subscribe(res => {
        if (res['status'] === 'success') {
          const toast = this.toastCtrl.create({
            message: 'Your Password has Been Updated',
            duration: 3000
          });
          toast.present();
        }

      }, () => {
        const toast = this.toastCtrl.create({
          message: 'Something Went Wrong',
          duration: 3000
        });
        toast.present();
      });
    } else {
      const toast = this.toastCtrl.create({
        message: 'Please Enter New Password',
        duration: 3000
      });
      toast.present();
    }
  }

  styleProgressBar(status) {

    if (status === 1) {
      return "active";
    }

    if (status === 0) {
      return "wrong";
    }
  }

  fileChange(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.img1 = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
    let file = event.target.files[0];
    let formData: FormData = new FormData();
    formData.append('file', file);
    console.log(formData);
    formData.append('upload_preset', 'dnavg0za');
    formData.append('api_key', '839396983344582');
    this.http.uploadFile(formData).subscribe(result => {
      this.img1 = result['url'];
      this.saveProfilePic({profile_image: result['url']})
      localStorage.setItem('profile_pic', result['url']);
    });
  }

  saveProfilePic(data) {
    const user = JSON.parse(localStorage.getItem('user'));
    data.id = user.id;
    this.http.updateProfilePic(data).subscribe(res => {

    });
  }

  getProgressPercent(total_q, right_q){
    let c = right_q*100/total_q;
    const t = c +'%';
    return t;
  }

       searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }

}
