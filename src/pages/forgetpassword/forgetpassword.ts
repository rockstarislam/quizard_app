import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Loading } from 'ionic-angular';
import { GivecodePage } from '../givecode/givecode';
import { HttpProvider } from '../../providers/http/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html',
})
export class ForgetpasswordPage {
  resetEmail = {email: ''};
  loading: Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private http: HttpProvider,
              private auth: AuthServiceProvider,) {
  }

  back() {
  	this.navCtrl.pop();
  }

  givecode() {
    this.navCtrl.push(GivecodePage);
  }

  public resetemail() {
    if(this.resetEmail.email){
    this.http.resEmail(this.resetEmail).subscribe(res => {
        if (res['response'] === 'success') {
          this.navCtrl.push(GivecodePage);
                    this.presentToast('We have send a reset code to your mail please Check');
        }else{
          this.presentToast('We can not find a user with that e-mail address');
        }
      },
      err => {
         this.loading.dismiss();
        //this.presentAlert('', err.error.error);
             const toast = this.toastCtrl.create({
            message: err.error.error,
            duration: 3000
          });
          toast.present();
      });
    }
    else{
      this.loading.dismiss();
        this.presentToast('Wrong Credential');
    }
  }

    presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000
    });
    toast.present();
  }

    showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpasswordPage');
  }

}
