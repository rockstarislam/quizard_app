import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfographicePage } from './infographice';

@NgModule({
  declarations: [
    InfographicePage,
  ],
  imports: [
    IonicPageModule.forChild(InfographicePage),
  ],
})
export class InfographicePageModule {}
