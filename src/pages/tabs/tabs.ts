import { Component } from '@angular/core';
import { ArticlePage } from '../article/article';
import { LeaderBoardPage } from '../leader-board/leader-board';
import { InfographicePage } from '../infographice/infographice';
import { SubjectListPage } from '../subject-list/subject-list';
import { QuizPage } from '../quiz/quiz';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = QuizPage;
  tab2Root = ArticlePage;
  tab3Root = InfographicePage;
  tab4Root = LeaderBoardPage;
  tab5Root = SubjectListPage;

  constructor() {

  }


goPage(page)
{
	console.log(page);
	switch (page) {
		case 1:
			 this.tab1Root = QuizPage;
			break;

				case 2:
			  this.tab2Root = ArticlePage;
			break;

				case 3:
		  this.tab3Root = InfographicePage;
			break;

				case 4:
  this.tab4Root = LeaderBoardPage;
			break;

				case 5:
  this.tab5Root = SubjectListPage;
			break;
		
		default:
			// code...
			break;
	}
}
}
