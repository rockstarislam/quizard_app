import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ArticlePage } from '../article/article';
 import { InfographicePage } from '../infographice/infographice';
 import { QuizPage } from '../quiz/quiz';

/**
 * Generated class for the SubjectListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subject-list',
  templateUrl: 'subject-list.html',
})
export class SubjectListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  quiz() {
  	this.navCtrl.push(QuizPage);
  }

   article() {
  	this.navCtrl.push(ArticlePage);
  }

  infographic() {
  	this.navCtrl.push(InfographicePage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubjectListPage');
  }

}
