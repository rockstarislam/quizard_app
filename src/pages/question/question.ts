import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import {ScorePage} from '../score/score';
import {HttpProvider} from '../../providers/http/http';
import {ProfilePage} from '../profile/profile';
import { SearchresultPage } from '../searchresult/searchresult';

/**
 * Generated class for the QuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-question',
  templateUrl: 'question.html',
})
export class QuestionPage {
  questions: any;
  quiz: any;
  selected_q: any;
  current_index: any;
  point: any;
  options: any;
  game_board: any;
  counter = 0;
  interValObj: any;
  is_started: boolean;
  minutes: any = 0;
  second: any = 0;
  img1: any;
  searchData: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: HttpProvider, private alertCtrl: AlertController) {
    this.questions = [];
    this.searchData = [];
    this.is_started = false;
    const data = JSON.parse(localStorage.getItem('user'));
    this.game_board = {
      user_id: data.id,
      total_question: 0,
      right_ans: 0,
      wrong_ans: 0,
      played_question_set: [],
      obtain_point: 0,
      quiz_id: this.navParams.data.id,
      quiz_title: this.navParams.data.quiz_title,
      played_time: 0
    };

    this.updateQuizStatus(this.navParams.data.id);


  }

  ionViewDidLeave() {
    // clearInterval(this.interValObj);
    clearInterval(this.interValObj);
    this.resetGame();
    this.counter = 0;
    this.is_started = false;
    this.minutes = 0;
    this.second = 0;
    this.current_index = 0;
  }

  score() {
    this.navCtrl.push(ScorePage);
  }


  ionViewDidLoad() {
    this.current_index = 0;
    this.getQList();
    if (localStorage.getItem('profile_pic')) {
      this.img1 = localStorage.getItem('profile_pic');
    }

  }

  getQList() {
    this.questions = [];
    this.quiz = this.navParams.data;
    this.questions = this.quiz.question;
    this.selected_q = this.questions[0];
    this.game_board.guiz_id = this.quiz.id;
    this.game_board.total_question = this.questions.length;
  }

  nextQ(id, ans) {
    if (this.counter === 0) {
      this.presentAlert('', 'Please start your quiz!');
      return false;
    }

    if (this.selected_q.id === id && Number(this.selected_q.option[0].answer) === ans) {
      // this.game_board.point = Number(this.game_board.point) + Number(this.selected_q.point);
      this.game_board.right_ans = this.game_board.right_ans + 1;
      this.selected_q.isRight = 1;
      this.game_board.played_question_set.push(this.selected_q);
      this.current_index = this.current_index + 1;
      this.getNextQ();
    }

    if (this.selected_q.id === id && Number(this.selected_q.option[0].answer) !== ans) {
      this.current_index = this.current_index + 1;
      this.game_board.wrong_ans = this.game_board.wrong_ans + 1;
      this.selected_q.isRight = 0;
      this.game_board.played_question_set.push(this.selected_q);
      this.getNextQ();
    }
  }

  getNextQ() {
    if ((this.questions.length) === this.current_index) {
      clearInterval(this.interValObj);
      this.game_board.played_time = this.counter;
      let score = Number(this.game_board.right_ans) * 5;
      const calculate = 12 * this.game_board.right_ans - this.counter;
      if (calculate > 0) {
        score = score + calculate;
      }

      this.game_board.obtain_point = score;

      if (localStorage.getItem('game_board')) {
        let data = JSON.parse(localStorage.getItem('game_board'));
        data.push(this.game_board);
        localStorage.setItem('game_board', JSON.stringify(data));
        this.navCtrl.push(ScorePage, this.game_board);
      } else {
        const data = [];
        data.push(this.game_board);
        localStorage.setItem('game_board', JSON.stringify(data));
        this.navCtrl.push(ScorePage, this.game_board);
      }
    } else {
      setTimeout(() => {
        this.options = "";
        this.selected_q = this.questions[this.current_index];
      }, 1000);
    }
  }

  startGame() {
    this.is_started = true;
    this.counter = 1;
    this.interValObj = setInterval(() => {
      this.counter = this.counter + 1;
      this.second = this.second + 1;


      if (this.second === 60) {
        this.minutes = this.minutes + 1;
        this.second = 0;
      }

      if (this.minutes === 2) {
        this.presentAlert('Alert', 'Game is over');
        clearInterval(this.interValObj);
        this.resetGame();
        this.getQList();
        this.counter = 0;
        this.is_started = false;
        return false;
      }
    }, 1000);
  }

  styleProgressBar(id) {
    if (this.game_board.played_question_set.length > 0) {
      const d = this.game_board.played_question_set.filter(item => item.id === id);
      if (d.length > 0) {
        if (d[0].isRight === 1) {
          return "active";
        }

        if (d[0].isRight === 0) {
          return "wrong";
        }
      }
    }
  }

  presentAlert(title, msg) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  resetGame() {
    const data = JSON.parse(localStorage.getItem('user'));
    this.game_board = {
      quiz_id: 0,
      user_id: data.id,
      total_question: 0,
      right_ans: 0,
      wrong_ans: 0,
      played_question_set: [],
      obtain_point: 0,
    };
  }

  updateQuizStatus(qid) {
    this.http.updateQuizViewStatus(qid).subscribe(() => {
    });
  }

     searchDetail(data) {
      if(data.type === 3){
       this.http.getQuizById(data.id).subscribe(res =>{
         console.log(res);
         this.navCtrl.push(QuestionPage, res[0]);
       });
      }else{
         this.navCtrl.push(SearchresultPage, data);
      }
  }

    searchNow($e) {
    this.searchData = [];
    this.http.searchApi($e.target.value).subscribe(output =>{
      this.searchData = output['data'];
    //  this.searchData.push(output.data);
    });
  }

  profile() {
    this.navCtrl.push(ProfilePage);
  }
}
