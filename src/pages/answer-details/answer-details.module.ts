import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnswerDetailsPage } from './answer-details';

@NgModule({
  declarations: [
    AnswerDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(AnswerDetailsPage),
  ],
})
export class AnswerDetailsPageModule {}
