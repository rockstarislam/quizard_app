import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import {SocialSharing} from '@ionic-native/social-sharing';
/**
 * Generated class for the SearchresultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searchresult',
  templateUrl: 'searchresult.html',
})
export class SearchresultPage {
  details: any;
  _imageViewerCtrl: ImageViewerController;
  constructor(public navCtrl: NavController, public navParams: NavParams, imageViewerCtrl: ImageViewerController, public socialSharing: SocialSharing) {
  	this._imageViewerCtrl = imageViewerCtrl;
  }

  ionViewDidLoad() {
    this.details = this.navParams.data;
  }

   fShare(item) {
    this.socialSharing.shareViaFacebook(item.description, item.image, '').then(() => {

    }).catch(() => {
    });
  }

  tShare(item) {
    this.socialSharing.shareViaTwitter(item.description, item.image, '').then(() => {

    }).catch(() => {
    });

  }

  eShare(item) {
// Share via email
    this.socialSharing.shareViaEmail(item.description, item.title, []).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

  wShare(item){
    this.socialSharing.share(item.description, item.title, item.image, '').then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

   presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
  }

}
